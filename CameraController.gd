extends Camera2D


const FOLLOW_SPEED = 4.0
const STATIC_SPEED = 1.0
const MAX_ZOOM = 0.7
const MAX_DISTANCE_FROM_PLAYER = 150

enum MODES { FOLLOWING_PLAYER, STATIC }
var node_to_follow: Node2D
var mode = MODES.FOLLOWING_PLAYER

func set_follow(node: Node2D):
	# print("setting follow to: " + node.name)
	node_to_follow = node

func set_static():
	mode = MODES.STATIC

# Called when the node enters the scene tree for the first time.
func _ready():
	current = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if node_to_follow:
		if mode == MODES.FOLLOWING_PLAYER:
			var player = node_to_follow
			var player_offset = player.get_parent().position

			var player_pos = player.global_position + player_offset

			var mouse_pos = get_global_mouse_position()
			var viewport_size = get_viewport_rect().end / 2

			# Vector2 between -1 and 1 in each direction showing how far the mouse moved
			var rel_mouse = (player_pos - mouse_pos) / viewport_size
			rel_mouse.limit_length(MAX_ZOOM)
			# print(rel_mouse)

			# var rel_mouse_pos = (player_pos - mouse_pos).limit_length(MAX_DISTANCE_FROM_PLAYER)

			var target_pos = player_pos - (rel_mouse * MAX_DISTANCE_FROM_PLAYER).limit_length(MAX_DISTANCE_FROM_PLAYER)

			# var target_pos = player_pos - rel_mouse_pos

			var current_distance = position.distance_to(player_pos)
			var dist_mod = 2.5 if current_distance > MAX_DISTANCE_FROM_PLAYER else 1.0

			position = position.linear_interpolate(target_pos, delta * FOLLOW_SPEED * dist_mod)

		elif mode == MODES.STATIC:
			var target_pos = node_to_follow.global_position
			position = position.linear_interpolate(target_pos, delta * STATIC_SPEED)

	else:
		push_warning("Camera has no node to follow, is this intentional?")
