extends KinematicBody2D

onready var strength_label = get_node("%StrengthNumber")
onready var times_hit_label = get_node("%TimesHitNumber")
onready var hud = $HUDContainer
onready var blood_particles = $BloodParticles
onready var nav_obstacle: NavigationObstacle2D = $NavigationObstacle2D

var times_hit = 0

var penetrating_projecitle: WeakRef

func _ready():
	# GODOT4: Workaround for godot 3, wait, till 4 realeaseses
	self.set_meta("penetrable", true)
	hud.visible = false

func on_penetration(projectile: Node2D):
	penetrating_projecitle = weakref(projectile)

func _process(delta):
	if not penetrating_projecitle:
		return

	var projectile = penetrating_projecitle.get_ref()
	if not projectile:
		return

	blood_particles.position = projectile.transform.get_origin()

func on_hit(damage, projectile: Node2D):
	strength_label.text = str(damage)
	times_hit += 1
	times_hit_label.text = str(times_hit)
	hud.visible = true

	if projectile.get_parent() == self:
		blood_particles.rotation = projectile.transform.get_rotation()
		blood_particles.position = to_local(projectile.transform.get_origin())
	else:
		blood_particles.rotation = projectile.rotation

	blood_particles.emitting = true
