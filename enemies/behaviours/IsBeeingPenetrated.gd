class_name IsBeeingPenetratedCondition
extends ConditionLeaf

func tick(actor, blackboard):
	if not actor.penetrating_projecitle:
		return FAILURE

	var projectile = actor.penetrating_projecitle.get_ref()
	if not projectile:
		return FAILURE

	return SUCCESS
