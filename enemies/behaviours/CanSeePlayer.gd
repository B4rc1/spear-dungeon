class_name CanSeePlayerCondition
extends ConditionLeaf

var player

func tick(actor, blackboard):
	var space_state = get_viewport().find_world_2d().direct_space_state
	var result = space_state.intersect_ray(actor.global_position, player.global_position,
							[actor])
	if result:
		if result.collider == player:
			return SUCCESS

	return FAILURE
