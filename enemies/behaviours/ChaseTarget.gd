class_name ChaseTargetAction
extends ActionLeaf

const CHASE_TO_DISTANCE = 50

const STEERING_STRENGTH = 3

var target: Node2D
var nav_agent: NavigationAgent2D

var velocity: Vector2 = Vector2.ZERO
var path_loc: Vector2


func tick(actor, blackboard):
	nav_agent = blackboard.get("nav_agent")

	if not nav_agent:
		push_error("[AI] no nav_agent on this node")

	if target == null or nav_agent == null:
		return FAILURE

	if not actor.sensing:
		push_error("[AI] this node does not have sensing")
		return FAILURE
		
	if target.global_position.distance_to(actor.global_position) <= CHASE_TO_DISTANCE:
		return SUCCESS

	nav_agent.set_target_location(target.global_position)

	var move_to = nav_agent.get_next_location()
	var pathfinding_dir = actor.to_local(move_to).normalized()
	
	# print(pathfinding_dir)

	path_loc = actor.to_local(move_to)

	var interest_arr = []
	for dir in actor.sensing.dir_arr:
		interest_arr.append(dir.dot(pathfinding_dir))

	var move_arr = []
	move_arr.resize(actor.sensing.SENSING_SIZE)

	for i in range(interest_arr.size()):
		move_arr[i] = interest_arr[i] - actor.sensing.sensing_arr[i]

	var max_idx = -1
	var max_val = -1
	var idx = 0
	for interest in move_arr:
		if interest > max_val:
			max_val = interest
			max_idx = idx
		idx += 1
	
	var desired_dir = actor.sensing.dir_arr[max_idx]

	#var desired_rot = Vector2.RIGHT.angle_to(desired_dir)

	var desired_vel = desired_dir * actor.MOVE_SPEED
	var steering_force = desired_vel - velocity

	velocity = velocity + (steering_force * blackboard.get('delta') * STEERING_STRENGTH)

	nav_agent.set_velocity(velocity)

	actor.look_at(target.global_position)
	
	actor.move_and_slide(velocity.rotated(actor.rotation))

	return RUNNING
	
