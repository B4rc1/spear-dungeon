class_name UpdateBloodParticlesAction
extends ActionLeaf

func tick(actor, blackboard):
	# This Check here is needed although we test it with IsBeeingPenetrated because
	# the reference can be freed by the engine whenever.
	if not actor.penetrating_projecitle:
		return FAILURE

	var projectile = actor.penetrating_projecitle.get_ref()
	if not projectile:
		return FAILURE

	actor.blood_particles.global_position = actor.penetrating_projecitle.get_ref().global_position
	print(actor.penetrating_projecitle.get_ref().global_position)
	return SUCCESS
