class_name IsDeadCondition
extends ConditionLeaf

func tick(actor, blackboard):
	return SUCCESS if actor.hp <= 0 else FAILURE
