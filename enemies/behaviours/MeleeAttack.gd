class_name MeleeAttackAction
extends ActionLeaf

export(float, 0, 2.0) var timer = 0.5

onready var attack_timer: Timer = Timer.new()

func _ready():
	attack_timer.wait_time = timer
	attack_timer.one_shot = true
	self.add_child(attack_timer)

var dmg = 1

func tick(actor, blackboard):
	var hitbox = blackboard.get("attack_hitbox")
	if not hitbox:
		push_error("[AI] no attack_hitbox on this node")
		return FAILURE

	if attack_timer.time_left > 0:
		return FAILURE

	var target = null
	var min_dist = 9999999999999
	for b in hitbox.get_overlapping_bodies():
		if not b.owner.has_method("on_hit") or b.owner == actor:
			continue

		var dist = actor.global_position.distance_to(b.owner.global_position)
		if dist < min_dist:
			target = b.owner
			min_dist = dist

	if not target:
		return FAILURE

	target.on_hit(1)
	attack_timer.start()
	return SUCCESS
