extends KinematicBody2D

onready var blood_particles = $BloodParticles
onready var nav_agent: NavigationAgent2D = $NavigationAgent2D
onready var nav_agent_dbg_line: Line2D = $NavigationAgent2D/NavigationDebugLine

onready var sensing = $Sensing

const MOVE_SPEED = 200

# FIXME: move this to blackboard?
var hp = 1

var penetrating_projecitle: WeakRef
var safe_velocity = Vector2.ZERO

func set_player(p):
	get_node("ZombieAI").set_player(p)

func _ready():
	# GODOT4: Workaround for godot 3, wait, till 4 realeaseses
	self.set_meta("penetrable", true)

	nav_agent.connect("velocity_computed", self, "_nav_agent_vel_computed")

func _nav_agent_vel_computed(vel):
	nav_agent_dbg_line.points = nav_agent.get_nav_path()
	# print("new vel computed ", vel)
	safe_velocity = vel

func on_hit(strength, projectile: Node2D):
	if projectile.get_parent() == self:
		blood_particles.rotation = projectile.transform.get_rotation()
		blood_particles.position = to_local(projectile.transform.get_origin())
	else:
		blood_particles.rotation = projectile.rotation

	blood_particles.emitting = true

	if strength >= Globals.Strength.High:

		nav_agent_dbg_line.visible = false

		hp -= 1

func on_penetration(projectile: Node2D):
	penetrating_projecitle = weakref(projectile)
