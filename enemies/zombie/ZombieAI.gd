extends BeehaveRoot

export var nav_agent_: NodePath
onready var nav_agent: = get_node(nav_agent_) as NavigationAgent2D

export var attack_hitbox_: NodePath
onready var attack_hitbox: = get_node(attack_hitbox_) as Area2D

# Called when the node enters the scene tree for the first time.
func _ready():
	# FIXME: horrible performance
	var player = Globals.get_player()

	blackboard.set("nav_agent", nav_agent)
	blackboard.set("attack_hitbox", attack_hitbox)

	$"%CanSeePlayer".player = player
	$"%ChasePlayer".target = player
