extends Node

onready var SENSING_SIZE = get_child_count()
onready var sensing_arr: PoolRealArray

var dir_arr = [ Vector2(0, 1), Vector2(1, 1).normalized(), Vector2(1, 0),\
				  Vector2(1, -1).normalized(), Vector2(0, -1), Vector2(-1, -1).normalized(),\
									Vector2(-1, 0), Vector2(-1, 1).normalized() ]

func _ready():
	sensing_arr.resize(SENSING_SIZE)
	sensing_arr.fill(0)

	for c in get_children():
		c.add_exception(get_parent())

func _physics_process(delta):
	sensing_arr.fill(0)

	for i in range(SENSING_SIZE):
		var ray = get_child(i)
		if ray.is_colliding():
			sensing_arr[i] += 5
			sensing_arr[(i - 1) % SENSING_SIZE] += 2
			sensing_arr[(i + 1) % SENSING_SIZE] += 2
	# print(sensing_arr)
