extends KinematicBody2D

onready var blood_particles = $BloodParticles


enum State { Chillin, LaunchedByProjectile, Stuck }
var penetrating_projecitle: WeakRef
var state = State.Chillin

func _ready():
	# GODOT4: Workaround for godot 3, wait, till 4 realeaseses
	self.set_meta("penetrable", true)

func on_penetration(projectile: Node2D):
	penetrating_projecitle = weakref(projectile)
	state = State.LaunchedByProjectile


func on_hit(_damage, projectile: Node2D):
	blood_particles.rotation = projectile.transform.get_rotation()
	blood_particles.position = to_local(projectile.transform.get_origin())
	blood_particles.emitting = true


func _process(_delta):
	if not penetrating_projecitle:
		return

	var projectile = penetrating_projecitle.get_ref()
	if not projectile:
		return

	if state == State.LaunchedByProjectile:
		blood_particles.position = projectile.transform.get_origin()
