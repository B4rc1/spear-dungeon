extends Node2D


onready var action_label = $InteractionTextContainer/ActionLabel
onready var interaction_text_container = $InteractionTextContainer
onready var blood_layer = $"Blood Layer"

onready var game_over_screen: CanvasLayer = $GameOverScreen

func show_interaction_label(node: Node2D):
	assert(node, "called show_interaction_label with empty node")

	var txt = node.get_meta("interaction_text")

	if not txt:
		txt = "[MISSING] interaction_text"

	interaction_text_container.visible = true
	action_label.text = txt

func hide_interaction_label():
	interaction_text_container.visible = false

func show_game_over_screen():
	game_over_screen.visible = true

func show_blood_layer():
	blood_layer.visible = true

func hide_blood_layer():
	blood_layer.visible = false

func _ready():
  interaction_text_container.visible = false
