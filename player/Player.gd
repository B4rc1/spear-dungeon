extends KinematicBody2D

export(Globals.Strength) var throw_strength

# ---- Node Refs ---- {{{
onready var cam = $"/root/CameraController"

onready var anim_player: AnimationPlayer = $AnimationPlayer
onready var interaction_area: Area2D = $InteractionArea2D
onready var dash_timer: Timer = $DashTimer
onready var gemstone = $Gemstone

onready var ui = get_node("../UI")
# }}}

# ---- Constants ---- {{{
const MOVE_SPEED = 300
const DASH_SPEED = MOVE_SPEED * 5
# }}}


# ---- Enums ---- {{{
enum STATE { IDLE, DEAD, DASHING }
# }}}

# ---- Variables {{{
var state = STATE.IDLE

var hp = 2

var current_weapon
var weapon_nodes: Array = []

# dash vars
var dash_dir: Vector2

# interaction vars
var current_interaction_node = null
var interaction_nodes: Array = []
# }}}

func set_node_enabled(node: Node, enabled: bool):
	node.visible = enabled
	node.set_process(enabled)
	node.set_physics_process(enabled)
	node.set_process_input(enabled)
	node.set_process_unhandled_input(enabled)

func _ready():
	cam.set_follow(self)


	var _a = interaction_area.connect("area_entered", self, "on_interaction_area_entered")
	_a = interaction_area.connect("area_exited", self, "on_interaction_area_exited")
	_a = dash_timer.connect("timeout", self, "reset_dash")

	Globals.set_player(self)
	yield(get_tree(), "idle_frame")
	get_tree().call_group("weapons", "set_player", self)
	get_tree().call_group("zombies", "set_player", self)

	weapon_nodes = $Weapons.get_children()
	for w in weapon_nodes:
		set_node_enabled(w, false)

	current_weapon = $Weapons/Sword
	set_node_enabled(current_weapon, true)


func reset_dash():
	if state == STATE.DASHING:
		state = STATE.IDLE

func on_hit(dmg):
	if state == STATE.DASHING:
		print("dodged with dash")
	print("recieved ", dmg, " damage")
	if hp - dmg <= 0:
		kill()
	else:
		hp -= dmg
		ui.show_blood_layer()

func _input(event):
	if event.is_action_pressed("restart") and state == STATE.DEAD:
		var _a = get_tree().reload_current_scene()
	elif event.is_action_pressed("interact"):
		interact()
	elif event.is_action_pressed("dash") and state == STATE.IDLE:
		dash_dir = get_move_vector()
		if not dash_dir:
			return

		state = STATE.DASHING
		dash_timer.start()



func interact():
	if current_interaction_node:
		var node = current_interaction_node

		node.on_interact(self)
		node.queue_free()

# FIXME: Interaction should be distance, not FIFO based.
func on_interaction_area_entered(area: Area2D):
	var node = area.get_parent()

	# print("can interact with: " + str(node))
	current_interaction_node = node

	if not interaction_nodes.has(node):
		interaction_nodes.append(node)

	ui.show_interaction_label(node)
	

func on_interaction_area_exited(area: Area2D):
	var node = area.get_parent()
	# print("can no longer interact with: " + str(node))

	# PERF: This can be optimized by figuring out the indexing
	#       this should always be `interaction_nodes[0]`, not 100% sure tho
	interaction_nodes.erase(node)

	if interaction_nodes.empty():
		current_interaction_node = null
		ui.hide_interaction_label()
	else:
		current_interaction_node = interaction_nodes.front()
		ui.show_interaction_label(current_interaction_node)


func get_move_vector() -> Vector2:
	var move_vec = Vector2()
	if Input.is_action_pressed("move_up"):
		move_vec.y -= 1
	if Input.is_action_pressed("move_down"):
		move_vec.y += 1
	if Input.is_action_pressed("move_left"):
		move_vec.x -= 1
	if Input.is_action_pressed("move_right"):
		move_vec.x += 1
	return move_vec.normalized()


func _physics_process(delta):
	if state == STATE.DEAD:
		return

	if state == STATE.DASHING:
		move_and_slide(dash_dir * MOVE_SPEED * 10)
		return

	
	var move_vec = get_move_vector()

	move_and_slide(move_vec * MOVE_SPEED)
	
	var look_vec = get_global_mouse_position() - global_position
	global_rotation = atan2(look_vec.y, look_vec.x)
	
func kill():
	interaction_area.monitoring = false
	cam.set_static()

	ui.hide_blood_layer()
	ui.show_game_over_screen()
	state = STATE.DEAD
