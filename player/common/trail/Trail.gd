extends Line2D


export var min_spawn_distance := 10.0
export var max_points = 20

var lifetime := [1.2, 1.6]
# var tick_speed := 0.01
# var tick := 0.0

onready var tween := $Decay
onready var parent := get_parent()

func _ready():
	set_as_toplevel(true)
	clear_points()
	var _a = tween.connect("tween_all_completed", self, "_on_Decay_tween_all_completed")

func stop():
	tween.interpolate_property(self, "modulate:a", 1.0, 0.0, rand_range(lifetime[0], lifetime[1]), Tween.TRANS_CIRC, Tween.EASE_OUT)
	tween.start()


func _process(_delta):
	# if tick > tick_speed:
	# 	tick = 0

	var pos = parent.global_position
	var n = get_point_count()

	if n > 0 and to_local(pos).distance_to( points[n-1] ) < min_spawn_distance:
		return

	if n < max_points:
		add_point(to_local(pos), 0)
	else:
		var old_points = points

		for p in range(1, n):
			points[p] = old_points[p - 1]

		points[0] = to_local(pos)
	# else:
	# 	tick += delta

func _on_Decay_tween_all_completed():
	queue_free()
