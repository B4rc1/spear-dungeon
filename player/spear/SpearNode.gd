extends KinematicBody2D

const SPEED = 800

enum State { Flying, Penetrating, InBody, Stuck}

var state = State.Flying

var velocity: Vector2 = Vector2.ZERO
# set from player when thrown
var throw_strength = Globals.Strength.Low

onready var trail = $Trail
onready var initial_hitbox: CollisionShape2D = get_node("%initial_hitbox")
onready var penetration_hitbox: CollisionShape2D = $PenetrationHitbox
# onready var pickup_area: Area2D = $PickupArea2D
onready var pickup_hitbox: CollisionShape2D = $PickupArea2D/PickupHitbox


onready var sprite: AnimatedSprite = $sprite


var node_stuck_in: Node2D

func on_interact(player):
	for w in player.weapon_nodes:
		if not w.get("num_spears") == null:
			w.num_spears += 1

# Called when the node enters the scene tree for the first time.
func _ready():
	self.set_meta("interaction_text", "pickup")
	var looking_vec = Vector2.RIGHT.rotated(rotation)

	var strength = Globals.strength_to_modifier(throw_strength)
	velocity = looking_vec * SPEED * strength
	print("[Spear] launching with strength %.1f, velocity: %s" % [strength, velocity])


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if state == State.Flying:
		velocity *= 0.995
		if velocity.length() < 200:
			transition_to_stuck()
			sprite.animation = "stuck_in_ground"
			state = State.Stuck

		# print("[Spear] velocity len: %s" % velocity.length())
		var cols = move_and_collide(velocity * delta)

		if cols:
			print("[Spear] initial collision with %s" % cols.collider)

			if cols.collider.has_method("on_hit"):
				cols.collider.on_hit(throw_strength, self)

			assert(cols.local_shape == initial_hitbox)
			initial_hitbox.disabled = true
			penetration_hitbox.disabled = false

			var remainder = cols.remainder
			var _cols2 = move_and_collide(remainder)
			# TODO: Handle multiple penetraiton

			state = State.Penetrating

			var old_transform = global_transform

			# if cols.collider.get_meta("penetrable", false):
			self.get_parent().remove_child(self)
			cols.collider.add_child(self)
			print("[Spear] attached to ", cols.collider)

			global_transform = old_transform

	elif state == State.Penetrating:
		var cols = move_and_collide(velocity * delta)
		if cols:
			node_stuck_in = cols.collider
			print("[Spear] Penetration collision with %s" % node_stuck_in)

			if throw_strength >= Globals.Strength.High\
				and node_stuck_in.has_method("on_penetration"):

				assert(node_stuck_in is KinematicBody2D,
					 "Collided with a node with on_penetration method that is not a KinematicBody2D")

				node_stuck_in.on_penetration(self)

				transition_to_stuck()
				state = State.InBody
			else:
				transition_to_stuck()
				state = State.Stuck

	elif state == State.InBody and node_stuck_in:
		velocity *= 0.98
		if velocity.length() < 150:
			print("[Spear] penetration stopped")
			state = State.Stuck

		var cols = node_stuck_in.move_and_collide(delta * velocity)
		# TODO: Multiple collisions

		if cols:
			state = State.Stuck


func transition_to_stuck():
	# FIXME: Ugly hack to disable trail
	trail.get_node("TrailRenderer").visible = false
	trail.set_process(false)

	penetration_hitbox.disabled = true
	pickup_hitbox.disabled = false
