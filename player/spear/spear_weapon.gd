extends Node

const CHARGING_ANIMATION_TIME = 0.8

enum STATE { READY, CHARGING, COOLDOWN }

var player: KinematicBody2D

var num_spears = 1
var charge_time: float = 0.0
var state = STATE.READY

# ---- Node Refs ---- {{{
onready var indicator: Node2D = $Indicator
onready var spear_spawn_loc: Node2D = $SpearSpawnLocation
onready var shoot_cooldown_timer: Timer = $ShootCooldownTimer
# }}}

# ---- Prelodaed Scenes ---- {{{
var spear_scene = preload("res://player/spear/SpearNode.tscn")
# }}}

func _ready():
	indicator.visible = false
	var _a = shoot_cooldown_timer.connect("timeout", self, "reset_cooldown")

func reset_cooldown():
	state = STATE.READY

func set_player(p):
	player = p

func _physics_process(delta):
	if (Input.is_action_just_pressed("shoot")
		and player.state == player.STATE.IDLE
		and state == STATE.READY
		and num_spears > 0):

		indicator.visible = true
		player.anim_player.play("spear_throw2")
		# var _a = throw_indicator_tween.interpolate_property(throw_indicator, "scale:x",
		# 																			null, 3, CHARGING_ANIMATION_TIME,
		# 																			Tween.TRANS_QUART, Tween.EASE_IN)
		#
		# _a = throw_indicator_tween.start()

		charge_time += delta
		state = STATE.CHARGING
		# var coll = raycast.get_collider()
		#if raycast.is_colliding() and coll.has_method("kill"):
		#	coll.kill()
	elif Input.is_action_pressed("shoot") and state == STATE.CHARGING:
		charge_time += delta

	elif Input.is_action_just_released("shoot") and state == STATE.CHARGING:
		# var _a = throw_indicator_tween.stop_all()
		indicator.visible = false

		player.anim_player.stop(true)
		# throw_indicator.visible = false
		# throw_indicator.scale.x = original_throw_indicator_scale_x

		shoot()
		shoot_cooldown_timer.start()




func shoot():
	# var strength = calc_dmg_multiplier(charge_time)

	# print("throwing spear with strenght: ", strength)

	var spear = spear_scene.instance()
	spear.position = spear_spawn_loc.global_position
	spear.rotation = player.rotation
	spear.throw_strength = player.throw_strength

	get_tree().get_root().add_child(spear)

	charge_time = 0
	num_spears -= 1



# Old Code, just here for reference
# const LOW_STRENGTH_MULTIPLIER     = 0.4
# const MID_STRENGTH_MULTIPLIER     = 0.6
# const HIGH_STRENGTH_MULTIPLIER    = 1.0
# const PERFECT_STRENGTH_MULTIPLIER = 1.2

# const LOW_STRENGTH_TIMING     = 0.20
# const MID_STRENGTH_TIMING     = 0.45
# const HIGH_STRENGTH_TIMING    = 0.75
# const PERFECT_STRENGTH_TIMING = 0.8


# enum Strength {Low, Mid, High, Perfect}



# func calc_dmg_multiplier():
# 	match throw_strength:
# 		Globals.Strength.Low: return Constants.LOW_STRENGTH .
# 		Globals.Strength.Mid: return Constants.MID_STRENGTH_MULTIPLIER
# 		Globals.Strength.High: return HIGH_STRENGTH_MULTIPLIER
# 		Globals.Strength.Perfect: return PERFECT_STRENGTH_MULTIPLIER

	# if throw_strength == Strength.Low:
	# 	return LOW_STRENGTH_TIMING

	# if time <= LOW_STRENGTH_TIMING:
	# 	return LOW_STRENGTH_MULTIPLIER
	#
	# elif time <= MID_STRENGTH_TIMING:
	# 	return MID_STRENGTH_MULTIPLIER
	#
	# elif time <= HIGH_STRENGTH_TIMING:
	# 	return HIGH_STRENGTH_MULTIPLIER
	#
	# elif time <= PERFECT_STRENGTH_TIMING:
	# 	return PERFECT_STRENGTH_MULTIPLIER
	# 
	# # Held for too long
	# else:
	# 	return HIGH_STRENGTH_MULTIPLIER

	# # See https://www.desmos.com/calculator/vwmrwm9jg6
	# if x <= 1:
	# 	return PERFECT_TIMING_MULTIPLIER * pow(x, 4)
	# else:
	# 	return max(1, (-PERFECT_TIMING_MULTIPLIER * pow(x, 4)) - (2 * PERFECT_TIMING_MULTIPLIER))
