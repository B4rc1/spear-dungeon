extends KinematicBody2D

var velocity: Vector2 = Vector2.ZERO

func on_interact(player):
	player.gemstone.has_gemstone = true

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	self.set_meta("interaction_text", "pickup gemstone")
	pass # Replace with function body.

func _physics_process(delta):
	move_and_slide(velocity)
