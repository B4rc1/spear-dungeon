extends Node2D

enum STATE { UP, COOLDOWN }

var player: KinematicBody2D

var state = STATE.UP

# --- Node refs --- {{{
onready var hit_area: Area2D = $SwordHitboxArea
onready var hit_collision: CollisionPolygon2D = $SwordHitboxArea/SwordCollisionPolygon
onready var attack_cooldown_timer: Timer = $AttackCooldownTimer
# }}}


# Called when the node enters the scene tree for the first time.
func _ready():
	var _a = hit_area.connect("body_entered", self, "on_hit")
	_a = attack_cooldown_timer.connect("timeout", self, "reset_cooldown")

func reset_cooldown():
	state = STATE.UP

func on_hit(body: Node):
	assert(body.has_method("on_hit"), "collided with enemy without on_hit() method")

	body.on_hit(Globals.Strength.High, player)

func set_player(p):
	player = p

func _physics_process(_delta):
	if Input.is_action_just_pressed("shoot") and player.state == player.STATE.IDLE and state == STATE.UP:
		player.anim_player.play("sword_attack")
		player.anim_player.connect("animation_finished", self,  "attack_anim_finished", [], CONNECT_ONESHOT)
		hit_collision.disabled = false
		state = STATE.COOLDOWN
		attack_cooldown_timer.start()

func attack_anim_finished(anim):
	assert(anim == "sword_attack", "[sword] some other animation has finished in anim player")
	hit_collision.disabled = true

	player.anim_player.play("sword_put_away");
