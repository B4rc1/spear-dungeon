extends Node2D

const FLYING_SPEED = 250

var has_gemstone = false

var gemstone_scene = preload("res://player/gem/GemstoneNode.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _input(event):
	if event.is_action_pressed("throw_gemstone") and has_gemstone:
		var gem = gemstone_scene.instance()
		# FIXME: This is fucking stupid, but the way the player nodes are structured, there is no other way :(
		gem.position = get_parent().position + get_parent().get_parent().position
		gem.velocity = Vector2.RIGHT.rotated(get_parent().rotation) * FLYING_SPEED
		gem.add_collision_exception_with(get_parent())
		get_tree().root.add_child(gem)
