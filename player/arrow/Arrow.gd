extends KinematicBody2D


const SPEED = 600

enum State { Flying, Stuck}

var state = State.Flying

onready var trail = $Trail
onready var collission_shape = $CollisionShape2D

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if state == State.Flying:
		var looking_vec = Vector2.RIGHT.rotated(rotation)
		var cols = move_and_collide(looking_vec * delta * SPEED)

		if cols:
			if cols.collider.has_method("on_hit"):
				cols.collider.on_hit(1, self.global_transform)


			trail.visible = false
			trail.set_process(false)

			collission_shape.disabled = true

			state = State.Stuck
