extends Node

# FIXME: Move Strength enum here
enum Strength {Low, Mid, High, Perfect}

var player: KinematicBody2D

func set_player(p):
	player = p

func get_player():
	return player

static func strength_to_modifier(strength):
	match strength:
		Strength.Low:     return 0.4
		Strength.Mid:     return 0.6
		Strength.High:    return 1.0
		Strength.Perfect: return 1.2
